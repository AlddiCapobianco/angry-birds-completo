using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReiniciarJuego : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            ReiniciarJuegoDesdeElPrincipio();
        }
    }

    public void ReiniciarJuegoDesdeElPrincipio()
    {
        SceneManager.LoadScene(0);
    }

}
