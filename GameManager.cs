using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject menuInicio;
    public GameObject contenido;
    public GameObject menuLevel;
    public float duracionVisible = 3.0f;
    public AudioClip SoundBoton;
    private AudioSource audioSource;
    public int level;
    public string siguienteNivel;
    public int numeroDeEnemigos;
    private int enemigosMuertos = 0;
    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        enemigosMuertos = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.X))
        {
            Camera.main.GetComponent<AudioSource>().PlayOneShot(SoundBoton);
            StartCoroutine(MostrarPantallaTemporal());
            contenido.SetActive(true);
            menuInicio.SetActive(false);
            Camera.main.GetComponent<AudioSource>().mute = false;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            #if UNITY_EDITOR
                EditorApplication.isPlaying = false;
            #else
                Application.Quit();
            #endif
        }
        
        if (Input.GetKeyDown(KeyCode.KeypadEnter))
        {
            SceneManager.LoadScene(0);
        }
    }

    private IEnumerator MostrarPantallaTemporal()
    {
        menuLevel.SetActive(true);
        yield return new WaitForSeconds(duracionVisible);
        menuLevel.SetActive(false);
    }

    public void EnemigoMuerto()
    {
        enemigosMuertos++;

        if (enemigosMuertos >= numeroDeEnemigos)
        {
            CargarSiguienteNivel();
        }
    }

    private void CargarSiguienteNivel()
    {
        if (!string.IsNullOrEmpty(siguienteNivel))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(siguienteNivel);
        }
    }
}

