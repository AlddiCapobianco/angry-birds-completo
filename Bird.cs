using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    private Rigidbody2D rb;
    private Camera mainCamera;
    Vector2 startPosition, clampedPosition;
    private Quaternion initialRotation;
    [SerializeField] private float force;
    [SerializeField] private float maxDistance;
    public AudioClip SoundThrow;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        mainCamera = Camera.main;
        startPosition = transform.position;
        audioSource = GetComponent<AudioSource>();
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        initialRotation = transform.rotation;
    }

    private void OnMouseDrag()
    {
        SetPosition();
    }

    private void SetPosition()
    {

        Vector2 dragPosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        clampedPosition = dragPosition;
        float dragDistance = Vector2.Distance(startPosition, dragPosition);
        if (dragDistance > maxDistance)
        {
            clampedPosition = startPosition + (dragPosition - startPosition).normalized * maxDistance;
        }
        if (dragPosition.x > startPosition.x)
        {
            clampedPosition.x = startPosition.x;
        }
        transform.position = clampedPosition;
    }
    private void OnMouseUp()
    {
        Throw();
    }

    private void Throw()
    {
        rb.isKinematic = false;
        Vector2 throwVector = startPosition - clampedPosition;
        rb.AddForce(throwVector * force);
        float resetTime = 5f;
        Invoke("Reset", resetTime);
        Camera.main.GetComponent<AudioSource>().PlayOneShot(SoundThrow);
        rb.constraints = RigidbodyConstraints2D.None;

    }

    private void Reset()
    {
        transform.position = startPosition;
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        mainCamera.GetComponent<CameraMovement>().ResetPosition();
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        ResetOrientation();
    }

    public void ResetOrientation()
    {
        transform.rotation = initialRotation;
    }
}
