using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    public AudioClip SoundCollision;
    private AudioSource audioSource;
    private GameManager gameManager;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameManager = GameObject.FindObjectOfType<GameManager>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ShouldDie(collision))
        {
            Destroy(gameObject);
            Camera.main.GetComponent<AudioSource>().PlayOneShot(SoundCollision);
            gameManager.EnemigoMuerto();
        }
    }

    private bool ShouldDie(Collision2D collision)
    {
        //COLLISION TRAE INFO DE LA COLISION, DE LA CUAL ACCEDEMOS AL OBJETO QUE OCASIONA
        //LA COLISION. VEMOS SI EN ESE OBJETO EXISTEUN COMPONENTE, EN ESTE CASO EL SCRIPT DEL BIRD
        bool isBird = collision.gameObject.GetComponent<Bird>();
        if (isBird)
        {
            return true;
        }
        
        float crushThreshold = 0.5f;
        //INFO DEL VECTOR NORMAL LA OBTENEMOS DEL PARAMETRO COLLISION, CON .CONTACTS[]
        //ES UN ARRAY DE PUNTO DE CONTACTO. Y DE SU ELEMNTO[0] (NORMALMENTE HAY 1 UNICO CONTACTO)
        //SU VECTOR NORMAL Y SU COMPONENTE Y
        bool isCrushed = collision.contacts[0].normal.y < -crushThreshold;
        if (isCrushed) 
        {
            return true;
        }
        return false;
    }
}

